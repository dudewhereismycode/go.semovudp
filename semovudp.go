package main

import (
"database/sql"
 	_ "github.com/go-sql-driver/mysql"
	"encoding/binary"
	//"encoding/hex"
	"flag"
	"fmt"
	//"log"
	"net"
	"github.com/eclipse/paho.mqtt.golang"
	//"io/ioutil"
	//"net/url"
	//"bytes"
	//"net/http"
	"time"
	"strings"
	"strconv"
	//"log"
	//"encoding/hex"
	"net/http"
	"log"
	"os"
)

var (
    // DBCon is the connection handle
    // for the database
    db *sql.DB
)


func FloatToString(input_num float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(input_num, 'f', 6, 64)
}
func publishMQTT(message string) {
	const TOPIC = "bridge/semov"

	opts := mqtt.NewClientOptions().AddBroker("tcp://bridge.dwim.mx:1883")
	opts.SetUsername("dwim")
	opts.SetPassword("gpscontrol1")
	opts.SetClientID("golang")
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
	}
	if token := client.Publish(TOPIC, 0, false, message); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
	}
	client.Disconnect(0)
	//fmt.Println("MQTT terminado")
}

func send2Mercury(imei string,eventcode string,latitude string, longitude string, speed string,heading string,altitude string){
    client := &http.Client{}
    req, err := http.NewRequest("GET", "https://mercury.dudewhereismy.com.mx/listener/semov", nil)
    if err != nil {
        log.Print(err)
        os.Exit(1)
    }

    q := req.URL.Query()
    q.Add("imei", imei)
    q.Add("event", eventcode)
    q.Add("latitude", latitude)
    q.Add("longitude", longitude)
    q.Add("speed",speed )
    q.Add("heading", heading )
    q.Add("altitude", altitude)
    q.Add("dvrurl", "")

    req.URL.RawQuery = q.Encode()
    fmt.Println("PAsed")
    fmt.Println(req.URL.String())
    resp, err := client.Do(req)

    if err != nil {
        fmt.Println("Errored when sending request to the server")
        return
    }

    defer resp.Body.Close()
}
func recProcess(line string) {

	i := strings.Index(line, "CCC")
	p := strings.Index(line, "AAA")
	if (i > -1) {
		// ZENDAS
		s := strings.Split(line, ",")
		//fmt.Println("len:",len(line))
		imei := s[1]
		placas:=m[imei]
		//fmt.Println("IMEI:",imei)
		//fmt.Println("CCC Index:",i)
		newstring := line[i + 4:]
		//fmt.Println("lenght:",len(newstring))
		//fmt.Println(hex.Dump([]byte(newstring)))
		ecf:=([]byte(newstring[8:9]))
		ec:=fmt.Sprintf("%d", ecf)
		eventcode:=strings.Replace(ec,"]","",-1)
		eventcode=strings.Replace(eventcode,"[","",-1)

		lat := binary.LittleEndian.Uint32([]byte(newstring[9:13]))
		//fmt.Println("latitude:",hex.Dump(latitude))
		var mylat = float64(lat) / 1000000.0
		var latitude = FloatToString(mylat)
		//fmt.Println("latitude:",latitude)
		//fmt.Println("longitude:",hex.Dump(longitude))
		lon := binary.LittleEndian.Uint32([]byte(newstring[13:17]))
		var flon = float64(lon)
		if (lon > 2147483647) {
			flon = (flon - 4294967296)
		}
		flon = (flon) / 1000000
		longitude := FloatToString(flon)
		//fmt.Println("Longitude:",longitude)
		utc := []byte(newstring[17:21])
		//fmt.Println("utc dump:",hex.Dump(utc))
		utc_time := binary.LittleEndian.Uint32(utc)
		//fmt.Println("UTC seconds:",utc_time)
		var nanoseconds = float64(utc_time)
		tutc := time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)
		tutc = tutc.Add(time.Duration(nanoseconds * 1000000000))
		//generationDate := fmt.Sprintf("%02d-%02d-%02dT%02d:%02d:%02d",
		//	tutc.Year(), tutc.Month(), tutc.Day(),
		//	tutc.Hour(), tutc.Minute(), tutc.Second())
		//generationTime := fmt.Sprintf("%02d:%02d:%02d", tutc.Hour(), tutc.Minute(), tutc.Second())
		//fmt.Println("Date:",generationDate)
		//speed
		mysp := []byte(newstring[24:26])
		//fmt.Println(hex.Dump(mysp))
		spe := binary.LittleEndian.Uint16(mysp)
		//spe:=0
		var fspe = float64(spe)
		//if (spe>2147483647) {
		//	fspe = (fspe - 4294967296)
		//				fspe=(fspe)/1000000
		speed := FloatToString(fspe)
		//fmt.Sprint(speed);
		myhea := []byte(newstring[26:28])
		//fmt.Println("heading:",hex.Dump(myhea))
		hea := binary.LittleEndian.Uint16(myhea)
		var fhea = float64(hea)
		//if  (hea>2147483647) {
		//	fhea = (fhea - 4294967296)
		//}
		//fhea=(fhea)/1000000
		heading := FloatToString(fhea)
		//fmt.Sprint(heading);
		myalt := []byte(newstring[32:34])
		//fmt.Println("Altitude:")
		//fmt.Println(hex.Dump(myalt))
		alt := binary.LittleEndian.Uint16(myalt)
		var falt = float64(alt)
		//if (alt>2147483647) {
		//	falt = (falt - 4294967296)
		//}
		//falt=(falt)/1000000
		altitude := FloatToString(falt)
		//fmt.Println("altitud:"+altitude)
		//soapSemov(eventcode,generationDate, generationTime, imei, altitude, latitude, longitude, speed, heading, "false", "true","zenda")
		if (eventcode == "35") {
			current := time.Now().UTC()
			receptionDate := fmt.Sprintf("%d/%02d/%02d %02d:%02d:%02d", current.Year(), current.Month(), current.Day(), current.Hour(), current.Minute(), current.Second())
			indate := "*" + receptionDate + " "
			data :="IMEI:"+imei+",UDP-ZENDA,"+eventcode+","+latitude+","+longitude+","+speed+","+heading+","+altitude+",PANIC:false"
			cmd := "update devices set lastupdate='" + receptionDate + "',plates='" + placas + "',event_code='" + eventcode + "',latitude='" + latitude + "',longitude='" + longitude + "',altitude='" + altitude + "',speed='" + speed + "',angle='" + heading + "',log='" + indate + data + "' where imei='" + imei + "';"
			db.Exec(cmd)
			publishMQTT(data)

		}
		if (eventcode == "1" || eventcode == "9") {
			//fmt.Println("SOS!!!")
			current := time.Now().UTC()
			receptionDate := fmt.Sprintf("%d/%02d/%02d %02d:%02d:%02d", current.Year(), current.Month(), current.Day(), current.Hour(), current.Minute(), current.Second())
			indate := "*" + receptionDate + " "
			data :="IMEI:"+imei+",UDP-ZENDA,"+eventcode+","+latitude+","+longitude+","+speed+","+heading+","+altitude+",PANIC:true"
			cmd := "update devices set lastupdate='" + receptionDate + "',panic='" + "true"+ "',plates='" + placas + "',event_code='" + eventcode + "',latitude='" + latitude + "',longitude='" + longitude + "',altitude='" + altitude + "',speed='" + speed + "',angle='" + heading + "',log='" + indate + data + "' where imei='" + imei + "';"
			db.Exec(cmd)
			publishMQTT(data)
			send2Mercury(imei,eventcode,latitude, longitude , speed ,heading ,altitude )

		}
		//insertSimple2("","",imei,"",latitude,longitude,"","",speed,heading,"","",generationDate,"",altitude,generationTime)
	}

	if (p > -1) {
		// MVT380 MVT100
		s := strings.Split(line, ",")
		//imei,eventcode,latitude,longitude,gps_utc,valid,speed,heading,mileage,analog:=s[1],s[3],s[4],s[5],s[6],s[7],s[10],s[11],s[14],s[18]
		//imei, eventcode, latitude, longitude, gps_utc, valid, speed, heading := s[1], s[3], s[4], s[5], s[6], s[7], s[10], s[11]
		imei,eventcode,latitude,longitude,valid,speed,heading,altitude:=s[1],s[3],s[4],s[5],s[7],s[10],s[11],s[13]
		placas:=m[imei]
		var km float64;
		//km,_= strconv.ParseFloat(mileage,64)
		km = km / 1000
		//var odometer int = int(km)
		//var smileage string=strconv.Itoa(odometer)
		//r:=strings.Split(analog,"|")
		//var b string=r[3]
		//x, err := strconv.ParseInt(b, 16, 64)
		//fbatt, err := strconv.ParseFloat(fmt.Sprintf("%d", x), 64)
		//var rbatt float64 = fbatt*3*2/1024
		//if err != nil {
		//	fmt.Printf("Error in conversion: %s\n", err)
		//} else {
		//	var battery=FloatToString(rbatt)
		//	battery="100"
		if valid == "A" {
			valid = "true"
		} else
		{
			valid = "false"
		}
		//if (imei== "864507035877413"){
		//fmt.Println("IMEI:",imei)
		//fmt.Println("Event:",eventcode)
		//}
		//user:="WS_TUC"
		//location:=""
		//fmt.Println("User:",user)
		//fmt.Println("IMEI:",imei)
		//fmt.Println("Event Code:",eventcode)
		//fmt.Println("latitude:",latitude)
		//fmt.Println("longitude:",longitude)
		//fmt.Println("Location:",location)
		//fmt.Println("utc:",gps_utc)
		//fmt.Println("valid:",valid)
		//fmt.Println("speed:",speed)
		//fmt.Println("Heading:",heading)
		////fmt.Println("altitude:",altitude)
		//fmt.Println("Mileage:",smileage)
		//fmt.Println("Battery:",battery)
		//	publishMQTT(line+" batt:"+battery)
		if (eventcode == "35") {
			current := time.Now().UTC()
			receptionDate := fmt.Sprintf("%d/%02d/%02d %02d:%02d:%02d", current.Year(), current.Month(), current.Day(), current.Hour(), current.Minute(), current.Second())
			indate := "*" + receptionDate + " "
			data :="IMEI:"+imei+",UDP-380,"+eventcode+","+latitude+","+longitude+","+speed+","+heading+","+altitude+",PANIC:false"
			cmd := "update devices set lastupdate='" + receptionDate + "',plates='" + placas + "',event_code='" + eventcode + "',latitude='" + latitude + "',longitude='" + longitude + "',altitude='" + altitude + "',speed='" + speed + "',angle='" + heading + "',log='" + indate + data + "' where imei='" + imei + "';"
			db.Exec(cmd)
                        //fmt.Println(cmd)
			publishMQTT(data)
/*			if (imei=="868998033124104"){
			fmt.Println(data)
				fmt.Println(cmd)
		        }*/
		}
		if (eventcode == "1" || eventcode == "9") {
			//fmt.Println("SOS!!!")
			current := time.Now().UTC()
			receptionDate := fmt.Sprintf("%d/%02d/%02d %02d:%02d:%02d", current.Year(), current.Month(), current.Day(), current.Hour(), current.Minute(), current.Second())
			indate := "*" + receptionDate + " "
			data :="IMEI:"+imei+",UDP-380,"+eventcode+","+latitude+","+longitude+","+speed+","+heading+","+altitude+",PANIC:true"
			cmd := "update devices set lastupdate='" + receptionDate + "',panic='" + "true"+ "',plates='" + placas + "',event_code='" + eventcode + "',latitude='" + latitude + "',longitude='" + longitude + "',altitude='" + altitude + "',speed='" + speed + "',angle='" + heading + "',log='" + indate + data + "' where imei='" + imei + "';"
			db.Exec(cmd)
			//fmt.Println(cmd)
			publishMQTT(data)
			send2Mercury(imei,eventcode,latitude, longitude , speed ,heading ,altitude )

		}

	}
}
var m = make(map[string]string)
var g = make(map[string]string)
var e = make(map[string]string)
var f = make(map[string]string)

func main() {

	var err error
	db, err = sql.Open("mysql", "gpscontrol:qazwsxedc@/bridge")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	flag.Parse()
	rows, err := db.Query("select imei,plates,mygroup,eco,url from devices where protocol=7")
	for rows.Next() {
		var imei string
		var plates string
		var mygroup string
		var eco string
		var url string
		err = rows.Scan(&imei, &plates, &mygroup, &eco, &url)
		//fmt.Println(imei)
		//fmt.Println(plates)
		//fmt.Println(mygroup)
		m[imei]=plates
		g[imei]=mygroup
		e[imei]=eco
		f[imei]=url
	}


	//Basic variables
	port := ":8532"
	protocol := "udp"

	//Build the address
	udpAddr, err := net.ResolveUDPAddr(protocol, port)
	if err != nil {
		fmt.Println("Wrong Address")
		return
	}

	//Output
	//fmt.Println("UDP\nReading " + protocol + " from " + udpAddr.String())

	//Create the connection
	udpConn, err := net.ListenUDP(protocol, udpAddr)
	if err != nil {
		fmt.Println(err)
	}

	//Keep calling this function
	for {
		display(udpConn)
	}

}


func display(conn *net.UDPConn) {

	var buf [2048]byte
	n, err := conn.Read(buf[0:])
	if err != nil {
		fmt.Println("Error Reading")
		return
	} else {
		//fmt.Println(hex.EncodeToString(buf[0:n]))
		//log.Printf("sent:\n%v", hex.Dump(buf[0:n]))
		go recProcess(string(buf[0:n]))
		//fmt.Println("Package Done")
	}

}
